#include <iostream>
#include <stdio.h>
#include <fstream> 
#include <cstring>
using namespace std;

int main(int argc,char* argv[])
{	
	int x,y,space1,space2;
	char fname[54];
	char type[] = ".nc";
	fstream file;
	
	cout << "Please enter in order    Number of X axis : ";
	cin >> x;
	cout << "\nPlease enter in order    Number of Y axis : ";
	cin >> y;
	cout << "\nPlease enter in order    X axis spacing(mm) : ";
	cin >> space1;
	cout << "\nPlease enter in order    Y axis spacing(mm) : ";
	cin >> space2;
	cout << "\n\nAbout naming rules:\n1.can not use spaces but can be replaced with characters (ex: _ or - )\n2.no need to enter .nc(ex: 5x5_array.nc just enter 5x5_array)\n\n ";
	cout << "\nPlease enter in order    file name : ";
	cin >> fname;
	cout << endl;
	strcat(fname,type); 
	
	file.open(fname , ios::out);
	if(!file)
	{
		cout << "File " << fname << "cannot open.\n";
		return 1;
	}
	else
		cout << "file create success.\n";
	
	
	file << "M05\n";
	file << "G00 X0 Y0\n\n";
	for(int i=0;i<x;i++)
	{
		for(int j=0;j<y;j++)
		{
			file<<"G00 "<< 'X' << i*space1 << ' '<< 'Y' << j*space2 << '\n';
			file << "M03 S30\nM05 S0\n";
		}
	}
	file << "G00 X0 Y0\n";
	file << "M30\n";
	file.close();
	

	return 0;
}
