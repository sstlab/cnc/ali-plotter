# Contributors

## Initial

[Cheng Shiu University](https://en.wikipedia.org/wiki/Cheng%20Shiu%20University), [Department of Mechanical Engineering](http://www.csu.edu.tw/csueng/academics/ce/mech.html):
* LAI Po-Chen (Allan)
* CHANG Wei-Chia (Kelly)
* YEN Tzu-Yi (Sharon)

[Sensor Technology Laboratory](http://sst.kmutt.ac.th) ([SST Wiki](http://sst.ipc.kmutt.ac.th/wiki))
* Chan LA-O-VORAKIAT
* Cristian F. GUAJARDO YÉVENES

## Current

